<%-- 
    Document   : form
    Created on : 16-may-2015, 17:40:23
    Author     : gianburga
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        
    </head>
    <body style="margin-top: 40px">
        <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h2>Responsable</h2></div>
                    <div class="panel-body">
                        <form:form method="post" action="" commandName="responsable">
                            <div class="form-group">
                                <label for="id_responsable_id">Id</label>
                                <form:input class="form-control" id="id_responsable_id" placeholder="" path="id"/>
                            </div>
                            <div class="form-group">
                                <label for="id_responsable_documento_identidad">Documento de identidad</label>
                                <form:input class="form-control" id="id_responsable_documento_identidad" placeholder="Ingresa un DNI" path="DNI"/>
                            </div>
                            <div class="form-group">
                                <label for="id_responsable_nombre">Nombre</label>
                                <form:input class="form-control" id="id_responsable_nombre" placeholder="Ingresa un nombre" path="Nombre"/>
                            </div>
                            <div class="form-group">
                                <label for="id_responsable_apellidos">Apellidos</label>
                                <form:input class="form-control" id="id_responsable_apellidos" placeholder="Ingresa unos apellidos" path="Apellido"/>
                            </div>
                            <button type="submit" class="btn btn-default">Submit</button>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </body>
</html>
