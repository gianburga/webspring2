/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myspring2.services;

import com.myspring2.beans.Responsable;
import com.myspring2.dao.ResponsableDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author gianburga
 */

@Service("responsableService")
public class ResponsableService {
    
    @Autowired
    private ResponsableDAO responsableDAO;
    
    public Responsable getById(Integer id){
        return getResponsableDAO().getById(id);
    }
    
    public List<Responsable> getAll(){
        return getResponsableDAO().getAll();
    }
    
    public void save(Responsable responsable){
        getResponsableDAO().save(responsable);
    }

    /**
     * @return the responsableDAO
     */
    public ResponsableDAO getResponsableDAO() {
        return responsableDAO;
    }

    /**
     * @param responsableDAO the responsableDAO to set
     */
    public void setResponsableDAO(ResponsableDAO responsableDAO) {
        this.responsableDAO = responsableDAO;
    }
}
