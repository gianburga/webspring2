package com.myspring2.controllers;


import com.myspring2.beans.Responsable;
import com.myspring2.services.ResponsableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author gianburga
 */

@Controller
@RequestMapping("/responsables")
public class ResponsableController {
    @Autowired
    private ResponsableService responsableService;
    
    @RequestMapping(method=RequestMethod.GET, value="/agregar")
    public ModelAndView agregar(){
        // Responsable responsable = responsableService.getById(1);
        return new ModelAndView(
                "responsables/form", 
                "responsable", new Responsable());
    }
    
    @RequestMapping(method=RequestMethod.POST, value="/agregar")
    public ModelAndView guardar(@ModelAttribute Responsable responsable){
        responsableService.save(responsable);
        return new ModelAndView("responsables/form", "responsable", responsable);
    }
}
