/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myspring2.dao;

import com.myspring2.beans.Tecnico;
import java.util.List;

/**
 *
 * @author gianburga
 */
public interface TecnicoDAO {
    public Tecnico getById(Integer id);
    public void save(Tecnico tenico);
    public List<Tecnico> getAll();
}
