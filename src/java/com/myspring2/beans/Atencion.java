/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myspring2.beans;

import java.sql.Date;

/**
 *
 * @author gianburga
 */
public class Atencion {
    private Integer id;
    private Empresa empresa;
    private Responsable responsable;
    private String ticket;
    private String detalle_atencion;
    private Date fecha_atencion;
    private Date fecha_creacion;
    private Integer estado;

    /**
     * @return the empresa
     */
    public Empresa getEmpresa() {
        return empresa;
    }

    /**
     * @param empresa the empresa to set
     */
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * @return the responsable
     */
    public Responsable getResponsable() {
        return responsable;
    }

    /**
     * @param responsable the responsable to set
     */
    public void setResponsable(Responsable responsable) {
        this.responsable = responsable;
    }

    /**
     * @return the ticket
     */
    public String getTicket() {
        return ticket;
    }

    /**
     * @param ticket the ticket to set
     */
    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    /**
     * @return the detalle_atencion
     */
    public String getDetalle_atencion() {
        return detalle_atencion;
    }

    /**
     * @param detalle_atencion the detalle_atencion to set
     */
    public void setDetalle_atencion(String detalle_atencion) {
        this.detalle_atencion = detalle_atencion;
    }

    /**
     * @return the fecha_atencion
     */
    public Date getFecha_atencion() {
        return fecha_atencion;
    }

    /**
     * @param fecha_atencion the fecha_atencion to set
     */
    public void setFecha_atencion(Date fecha_atencion) {
        this.fecha_atencion = fecha_atencion;
    }

    /**
     * @return the fecha_creacion
     */
    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    /**
     * @param fecha_creacion the fecha_creacion to set
     */
    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * @return the estado
     */
    public Integer getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    
}
